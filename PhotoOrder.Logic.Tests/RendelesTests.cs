﻿// <copyright file="RendelesTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using PhotoOrder.Data;
    using PhotoOrder.Repository;

    /// <summary>
    /// A rendelések tesztelése.
    /// </summary>
    [TestFixture]
    public class RendelesTests
    {
        /// <summary>
        /// Teszteli a rendelés mennyiségének helyes értékkel frissítését.
        /// </summary>
        [Test]
        public void TestUpdateMennyiseg()
        {
            // Mock felépítése
            Mock<IRendelesRepository> rendMock = new Mock<IRendelesRepository>();

            List<Rendeles> rendelesek = new List<Rendeles>()
            {
                new Rendeles() { rendeles_id = 1, darabszam = 1 },
                new Rendeles() { rendeles_id = 2, darabszam = 5 },
                new Rendeles() { rendeles_id = 3, darabszam = 6 },
            };

            // Logika végrehajtása
            PhotoOrderLogic logic = new PhotoOrderLogic() { RendelesRepository = rendMock.Object };
            logic.UpdateRendelesDarabszam(2, 3);

            // Eredmény tesztelése
            rendMock.Verify(repo => repo.UpdateMennyiseg(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Teszteli, hogy hibás darabszám megadása esetén kivételt dob-e a program.
        /// </summary>
        /// <param name="darabszam">A hibás darabszám.</param>
        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-3)]
        public void TestUpdateMennyisegThrowsException(int darabszam)
        {
            Mock<IRendelesRepository> rendMock = new Mock<IRendelesRepository>();
            List<Rendeles> rendelesek = new List<Rendeles>()
            {
                new Rendeles() { rendeles_id = 1, darabszam = 1 },
                new Rendeles() { rendeles_id = 2, darabszam = 5 },
                new Rendeles() { rendeles_id = 3, darabszam = 6 },
            };

            PhotoOrderLogic logic = new PhotoOrderLogic() { RendelesRepository = rendMock.Object };

            // Hibát dob?
            Assert.That(() => logic.UpdateRendelesDarabszam(1, darabszam), Throws.ArgumentException);

            rendMock.Verify(repo => repo.UpdateMennyiseg(It.IsAny<int>(), It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// Teszteli az új rendlések hozzáadását.
        /// </summary>
        [Test]
        public void TesztAddRendeles()
        {
            Mock<IRendelesRepository> rendMock = new Mock<IRendelesRepository>();
            PhotoOrderLogic logic = new PhotoOrderLogic() { RendelesRepository = rendMock.Object };

            Rendeles ujReneles = new Rendeles() { rendeles_id = 1 };
            logic.AddRendeles(ujReneles);

            rendMock.Verify(repo => repo.Insert(ujReneles), Times.Once);
        }

        /// <summary>
        /// Teszteli a rendelések törlését.
        /// </summary>
        [Test]
        public void TesztDeleteRendeles()
        {
            Mock<IRendelesRepository> rendMock = new Mock<IRendelesRepository>();

            Rendeles torlendo = new Rendeles() { rendeles_id = 3, darabszam = 6 };

            List<Rendeles> rendelesek = new List<Rendeles>()
            {
                new Rendeles() { rendeles_id = 1, darabszam = 1 },
                new Rendeles() { rendeles_id = 2, darabszam = 5 },
            };

            rendelesek.Add(torlendo);

            PhotoOrderLogic logic = new PhotoOrderLogic() { RendelesRepository = rendMock.Object };

            logic.DeleteRendeles(torlendo);

            rendMock.Verify(repo => repo.Remove(torlendo), Times.Once);
            rendMock.Verify(repo => repo.GetAll(), Times.Never);
        }
    }
}
