﻿// <copyright file="RendelesInfoTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using PhotoOrder.Data;
    using PhotoOrder.Repository;

    /// <summary>
    /// A rendelés infók tesztelése.
    /// </summary>
    [TestFixture]
    public class RendelesInfoTests
    {
        /// <summary>
        /// Rendlési infó hozzáadásának tesztelése.
        /// </summary>
        [Test]
        public void TestAddRendelesInfo()
        {
            Mock<IRendelesInfoRepository> rendInfoMock = new Mock<IRendelesInfoRepository>();
            PhotoOrderLogic logic = new PhotoOrderLogic() { RendelesInfoRepository = rendInfoMock.Object };

            Rendeles_info rendelesInfo = new Rendeles_info() { vezeteknev = "Török", keresztnev = "Tamás" };
            logic.AddRendelesInfo(rendelesInfo);

            rendInfoMock.Verify(repo => repo.Insert(rendelesInfo), Times.Once);
        }

        /// <summary>
        /// Rendlési infó hozzáadásának tesztelése.
        /// </summary>
        [Test]
        public void TestDeleteRendelesInfo()
        {
            Mock<IRendelesInfoRepository> rendInfoMock = new Mock<IRendelesInfoRepository>();
            PhotoOrderLogic logic = new PhotoOrderLogic() { RendelesInfoRepository = rendInfoMock.Object };

            logic.DeleteRendelesInfo(new Rendeles_info());

            rendInfoMock.Verify(repo => repo.Remove(It.IsAny<Rendeles_info>()), Times.Once);
        }

        /// <summary>
        /// Az osztályok módosításának tesztelése.
        /// </summary>
        [Test]
        public void TestUpdateOsztaly()
        {
            Mock<IRendelesInfoRepository> rendInfoMock = new Mock<IRendelesInfoRepository>();
            PhotoOrderLogic logic = new PhotoOrderLogic() { RendelesInfoRepository = rendInfoMock.Object };

            List<Rendeles_info> rendinfok = new List<Rendeles_info>()
            {
                new Rendeles_info() { rendeles_id = 2 },
            };
            logic.UpdateRendelesInfoOsztaly(2, "1.A");

            rendInfoMock.Verify(repo => repo.UpdateOsztaly(2, "1.A"), Times.Once);
            rendInfoMock.Verify(repo => repo.Remove(It.IsAny<Rendeles_info>()), Times.Never);
        }
    }
}
