﻿// <copyright file="KepTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using PhotoOrder.Data;
    using PhotoOrder.Repository;

    /// <summary>
    /// A képek logikáját teszteli.
    /// </summary>
    [TestFixture]
    public class KepTests
    {
        /// <summary>
        /// Rendlési infó hozzáadásának tesztelése.
        /// </summary>
        [Test]
        public void TestAddKep()
        {
            Mock<IKepRepository> kepRepo = new Mock<IKepRepository>();
            PhotoOrderLogic logic = new PhotoOrderLogic() { KepRepository = kepRepo.Object };

            Kep uJKep = new Kep() { fotos = "Vörös Nándor", rendelesi_kod = "EDH-40KPHT" };
            logic.AddKep(uJKep);

            kepRepo.Verify(repo => repo.Insert(uJKep), Times.Once);
        }

        /// <summary>
        /// Rendlési infó hozzáadásának tesztelése.
        /// </summary>
        [Test]
        public void TestDeleteKep()
        {
            Mock<IKepRepository> kepRepo = new Mock<IKepRepository>();
            PhotoOrderLogic logic = new PhotoOrderLogic() { KepRepository = kepRepo.Object };

            logic.DeleteKep(new Kep());

            kepRepo.Verify(repo => repo.Remove(It.IsAny<Kep>()), Times.Once);
        }

        /// <summary>
        /// Test kep elforgatasa.
        /// </summary>
        [Test]
        public void TestKepElforgatasa()
        {
            Mock<IKepRepository> kepRepo = new Mock<IKepRepository>();
            PhotoOrderLogic logic = new PhotoOrderLogic() { KepRepository = kepRepo.Object };

            List<Kep> kepek = new List<Kep>()
            {
                new Kep() { kep_id = 6, tajolas = 0 },
            };

            kepRepo.Setup(x => x.GetAll()).Returns(kepek.AsQueryable);

            logic.KepElforgat(6);

            kepRepo.Verify(repo => repo.Remove(It.IsAny<Kep>()), Times.Never);
            kepRepo.Verify(repo => repo.Elforgat(6), Times.Once);
        }

        /// <summary>
        /// Test kep elforgatasa.
        /// </summary>
        [Test]
        public void TestGetKepekByFotos()
        {
            Mock<IKepRepository> kepRepo = new Mock<IKepRepository>();

            List<Kep> kepek = new List<Kep>()
            {
                new Kep() { fotos = "Vörös Nándor", kep_id = 1 },
                new Kep() { fotos = "Török Tamás", kep_id = 2 },
                new Kep() { fotos = "Török Tamás", kep_id = 3 },
                new Kep() { fotos = "Toby Stark", kep_id = 4 },
                new Kep() { fotos = "Vörös Nándor", kep_id = 5 },
            };

            List<Kep> expected = new List<Kep>()
            {
                new Kep() { fotos = "Vörös Nándor", kep_id = 1 },
                new Kep() { fotos = "Vörös Nándor", kep_id = 5 },
            };

            kepRepo.Setup(repo => repo.GetAll()).Returns(kepek.AsQueryable());

            PhotoOrderLogic logic = new PhotoOrderLogic() { KepRepository = kepRepo.Object };

            List<Kep> result = logic.GetKepekByFotos("Vörös Nándor").ToList();

            kepRepo.Verify(repo => repo.GetAll(), Times.Once);

            Assert.That(result.Select(x => x.kep_id), Does.Contain(1));
            Assert.That(result.Select(x => x.kep_id), Does.Contain(5));
            Assert.That(expected.Count, Is.EqualTo(result.Count));
        }
    }
}
