﻿// <copyright file="SzolgaltatasTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using NUnit.Framework.Internal;
    using PhotoOrder.Data;
    using PhotoOrder.Repository;

    /// <summary>
    /// A szolgáltatások logikájának tesztjei.
    /// </summary>
    [TestFixture]
    public class SzolgaltatasTests
    {
        /// <summary>
        /// Teszteli az j szolgáltaások hozzáadását.
        /// </summary>
        [Test]
        public void TesztAddSzolgaltatas()
        {
            Mock<ISzolgaltatasRepository> szolgMock = new Mock<ISzolgaltatasRepository>();
            PhotoOrderLogic logic = new PhotoOrderLogic() { SzolgaltatasRepository = szolgMock.Object };

            Szolgaltatas szolg = new Szolgaltatas() { nev = "Poster" };
            logic.AddSzolgaltatas(szolg);

            szolgMock.Verify(repo => repo.Insert(szolg), Times.Once);
        }

        /// <summary>
        /// Teszteli a szolgáltatás törlő függvényt.
        /// </summary>
        [Test]
        public void TestDeleteSzolgaltatas()
        {
            Mock<ISzolgaltatasRepository> szolgMock = new Mock<ISzolgaltatasRepository>();
            PhotoOrderLogic logic = new PhotoOrderLogic() { SzolgaltatasRepository = szolgMock.Object };

            Szolgaltatas szolg = new Szolgaltatas() { nev = "Poster" };
            logic.DeleteSzolgaltatas(szolg);

            szolgMock.Verify(repo => repo.Remove(szolg), Times.Once);
        }

        /// <summary>
        /// Teszteli a szolgáltatások egységárának módosítását.
        /// </summary>
        [Test]
        public void TestUpdateEgysegar()
        {
            Mock<ISzolgaltatasRepository> szolgMock = new Mock<ISzolgaltatasRepository>();
            PhotoOrderLogic logic = new PhotoOrderLogic() { SzolgaltatasRepository = szolgMock.Object };

            List<Szolgaltatas> szolg = new List<Szolgaltatas>()
                { new Szolgaltatas() { szolgaltatas_id = 4, nev = "Poster" } };

            szolgMock.Setup(x => x.GetAll()).Returns(szolg.AsQueryable);
            logic.UpdateSzolgaltatasEgysegar(4, 1000);

            szolgMock.Verify(repo => repo.UpdateEgysegar(4, 1000), Times.Once);
            szolgMock.Verify(repo => repo.GetAll(), Times.Never);
        }
    }
}
