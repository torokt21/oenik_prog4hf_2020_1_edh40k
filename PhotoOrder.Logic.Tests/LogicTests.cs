﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using PhotoOrder.Data;
    using PhotoOrder.Repository;

    /// <summary>
    /// A több modelt összekötő logikát tesztelő függvények gyűjtőhelye.
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        /// <summary>
        /// Leteszteli, hogy helyesen számolja-e össze a logika egy rendelés árát.
        /// </summary>
        [Test]
        public void TesztRendelesOsszarSzamolasa()
        {
            Mock<IRendelesRepository> rendelesMock = new Mock<IRendelesRepository>();

            List<Szolgaltatas> szolgaltasok = new List<Szolgaltatas>()
            {
                new Szolgaltatas() { szolgaltatas_id = 1, egysegar = 500 },
                new Szolgaltatas() { szolgaltatas_id = 2, egysegar = 200 },
            };

            List<Rendeles> rendelesek = new List<Rendeles>()
            {
                new Rendeles() // 2 x 500
                {
                    rendeles_id = 1,
                    darabszam = 2,
                    Szolgaltatas = szolgaltasok[0],
                },
                new Rendeles() // 3 x 200
                {
                    rendeles_id = 1,
                    darabszam = 3,
                    Szolgaltatas = szolgaltasok[1],
                },
                new Rendeles() // Nem a vizsgált rendeléshez tartozik, így a logika nem szabad hogy figyelembe vegye.
                {
                    rendeles_id = 2,
                    darabszam = 30,
                    Szolgaltatas = szolgaltasok[1],
                },
            };

            rendelesMock.Setup(x => x.GetAll()).Returns(rendelesek.AsQueryable);

            PhotoOrderLogic logic = new PhotoOrderLogic() { RendelesRepository = rendelesMock.Object };

            Assert.That(logic.GetRendelesOsszar(1), Is.EqualTo(1600));
            rendelesMock.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Leteszteli, hogy ha egy nem létező rendelés azonosítót akarunk összeszámolni, kivétel keletkezik.
        /// </summary>
        [Test]
        public void TesztRendelesOsszarSzamolasa_KivetelHaNincsOlyanRendelés()
        {
            Mock<IRendelesRepository> rendelesMock = new Mock<IRendelesRepository>();

            List<Rendeles> rendelesek = new List<Rendeles>()
            {
                new Rendeles()
                {
                    rendeles_id = 1,
                },
                new Rendeles()
                {
                    rendeles_id = 1,
                },
                new Rendeles()
                {
                    rendeles_id = 2,
                },
                new Rendeles()
                {
                    rendeles_id = 4,
                },
            };

            rendelesMock.Setup(x => x.GetAll()).Returns(rendelesek.AsQueryable);

            PhotoOrderLogic logic = new PhotoOrderLogic() { RendelesRepository = rendelesMock.Object };

            Assert.Throws<KeyNotFoundException>(() => logic.GetRendelesOsszar(3));
        }

        /// <summary>
        /// Leteszteli, hogy helyesen számolja-e össze a logika egy rendelés árát.
        /// </summary>
        [Test]
        public void TesztRendelesekAtlaganakSzamolasa()
        {
            Mock<IRendelesInfoRepository> rendelesInfoMock = new Mock<IRendelesInfoRepository>();

            List<Szolgaltatas> szolgaltasok = new List<Szolgaltatas>()
            {
                new Szolgaltatas() { szolgaltatas_id = 1, egysegar = 100 },
                new Szolgaltatas() { szolgaltatas_id = 2, egysegar = 200 },
                new Szolgaltatas() { szolgaltatas_id = 3, egysegar = 300 },
            };

            List<Rendeles_info> rendelesInfok = new List<Rendeles_info>() // Teljes sum: 6 900
                {
                    new Rendeles_info()
                    {
                        Rendeles = new List<Rendeles>() // Sum: 4200
                        {
                            new Rendeles() // 10 x 300
                            {
                                darabszam = 10,
                                Szolgaltatas = szolgaltasok[2],
                            },
                            new Rendeles() // 5 x 200
                            {
                                darabszam = 5,
                                Szolgaltatas = szolgaltasok[1],
                            },
                            new Rendeles() // 2 x 100
                            {
                                darabszam = 2,
                                Szolgaltatas = szolgaltasok[0],
                            },
                        },
                    },
                    new Rendeles_info()
                    {
                        Rendeles = new List<Rendeles>() // Sum: 900
                        {
                            new Rendeles() // 1 x 100
                            {
                                darabszam = 1,
                                Szolgaltatas = szolgaltasok[0],
                            },
                            new Rendeles() // 4 x 200
                            {
                                darabszam = 4,
                                Szolgaltatas = szolgaltasok[1],
                            },
                        },
                    },
                    new Rendeles_info()
                    {
                        Rendeles = new List<Rendeles>() // Sum: 1800
                        {
                            new Rendeles() // 3 x 200
                            {
                                darabszam = 3,
                                Szolgaltatas = szolgaltasok[1],
                            },
                            new Rendeles() // 4 x 300
                            {
                                darabszam = 4,
                                Szolgaltatas = szolgaltasok[2],
                            },
                        },
                    },
                };

            rendelesInfoMock.Setup(x => x.GetAll()).Returns(rendelesInfok.AsQueryable);

            PhotoOrderLogic logic = new PhotoOrderLogic() { RendelesInfoRepository = rendelesInfoMock.Object };

            Assert.That(logic.GetRendelesekAtlaga(), Is.EqualTo(2300));
            rendelesInfoMock.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Leteszteli, hogy helyesen számolja-e össze a logika egy rendelés árát.
        /// </summary>
        [Test]
        public void TesztSzolgáltatasokDarabszamraBontasa()
        {
            Mock<IRendelesRepository> rendelesMock = new Mock<IRendelesRepository>();

            List<Szolgaltatas> szolgaltasok = new List<Szolgaltatas>()
            {
                new Szolgaltatas() { szolgaltatas_id = 1 },
                new Szolgaltatas() { szolgaltatas_id = 2 },
                new Szolgaltatas() { szolgaltatas_id = 3 },
                new Szolgaltatas() { szolgaltatas_id = 4 },
            };

            List<Rendeles> rendelesek = new List<Rendeles>()
            {
                new Rendeles()
                {
                    darabszam = 2,
                    Szolgaltatas = szolgaltasok[0],
                },
                new Rendeles()
                {
                    darabszam = 5,
                    Szolgaltatas = szolgaltasok[3],
                },
                new Rendeles()
                {
                    darabszam = 3,
                    Szolgaltatas = szolgaltasok[1],
                },
                new Rendeles()
                {
                    darabszam = 10,
                    Szolgaltatas = szolgaltasok[1],
                },
                new Rendeles()
                {
                    darabszam = 3,
                    Szolgaltatas = szolgaltasok[0],
                },
                new Rendeles()
                {
                    darabszam = 1,
                    Szolgaltatas = szolgaltasok[2],
                },
                new Rendeles()
                {
                    darabszam = 3,
                    Szolgaltatas = szolgaltasok[0],
                },
                new Rendeles()
                {
                    darabszam = 4,
                    Szolgaltatas = szolgaltasok[3],
                },
            };

            rendelesMock.Setup(x => x.GetAll()).Returns(rendelesek.AsQueryable);

            PhotoOrderLogic logic = new PhotoOrderLogic() { RendelesRepository = rendelesMock.Object };

            List<SzolgaltasasDarabItem> expected = new List<SzolgaltasasDarabItem>()
            {
                new SzolgaltasasDarabItem() { Szolgaltatas = szolgaltasok[0], Darab = 8 },
                new SzolgaltasasDarabItem() { Szolgaltatas = szolgaltasok[1], Darab = 13 },
                new SzolgaltasasDarabItem() { Szolgaltatas = szolgaltasok[2], Darab = 1 },
                new SzolgaltasasDarabItem() { Szolgaltatas = szolgaltasok[3], Darab = 9 },
            };

            List<SzolgaltasasDarabItem> result = logic.GetDarabszamokBySzolgaltatas().ToList();

            Assert.That(result, Is.EquivalentTo(expected));
            rendelesMock.Verify(repo => repo.GetAll(), Times.Once);
        }
    }
}
