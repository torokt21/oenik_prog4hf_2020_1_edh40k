﻿using AutoMapper;
using PhotoOrder.Data;
using PhotoOrder.Logic;
using PhotoOrder.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PhotoOrder.Web.Controllers
{
    public class KepApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        ILogic logic;
        IMapper mapper;

        public KepApiController()
        {
            this.logic = new PhotoOrderLogic(new PhotoOrderModel());
            this.mapper = MapperFactory.CreateMapper();
        }

        // GET api/KepApi/all
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Kep> GetAll()
        {
            var kepek = logic.GetKepek().ToList();
            return mapper.Map<IList<Data.Kep>, List<Models.Kep>>(kepek);
        }

        // GET api/KepApi/delete
        [ActionName("delete")]
        [HttpGet]
        public ApiResult Delete(int id)
        {
            var kep = logic.GetKepek().FirstOrDefault(k => k.kep_id == id);

            bool success = false;
            if (kep == default)
                success = false;
            else
                logic.DeleteKep(kep);

            return new ApiResult() { OperationResult = success };
        }

        // GET api/KepApi/add
        [ActionName("add")]
        [HttpPost]
        public ApiResult Add(Models.Kep kep)
        {
            // Using the mapper to convert back
            Data.Kep dKep = mapper.Map<Models.Kep, Data.Kep>(kep);

            logic.AddKep(dKep);

            return new ApiResult() { OperationResult = true };
        }

        // GET api/KepApi/update
        [ActionName("update")]
        [HttpPost]
        public ApiResult Update(Models.Kep kep)
        {
            // Using the mapper to convert back
            Data.Kep dKep = mapper.Map<Models.Kep, Data.Kep>(kep);

            bool success = logic.UpdateKep(dKep.kep_id, dKep.rendelesi_kod, dKep.eleresi_ut, dKep.fotos, (decimal)dKep.tajolas);

            return new ApiResult() { OperationResult = success };
        }
    }
}
