﻿using AutoMapper;
using PhotoOrder.Data;
using PhotoOrder.Logic;
using PhotoOrder.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhotoOrder.Web.Controllers
{
    public class KepController : Controller
    {
        ILogic logic;
        IMapper mapper;
        KepViewModel vm;

        public KepController()
        {
            this.logic = new PhotoOrderLogic(new PhotoOrderModel());
            this.mapper = MapperFactory.CreateMapper();

            this.vm = new KepViewModel();
            vm.EditedKep = new Models.Kep();

            // Data.képek lekérése
            var Kepek = logic.GetKepek().ToList();

            // Átkonvertálás form modelekké
            vm.Kepek = mapper.Map<IList<Data.Kep>, List<Models.Kep>>(Kepek);

        }

        /// <summary>
        /// A data PhotoOrder.Web.képekből a PhotoOrder.Web.képeket csinál.
        /// </summary>
        /// <param name="id">A kép id-je</param>
        /// <returns>Visszaad egy képet.</returns>
        private Models.Kep GetKepModel(int id)
        {
            var kep = logic.GetKepek().FirstOrDefault(k => k.kep_id == id);
            return this.mapper.Map<Data.Kep, Models.Kep>(kep);
        }

        // GET: Kep
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("KepIndex", this.vm);
        }

        // GET: Kep/Details/5
        public ActionResult Details(int id)
        {
            return View("KepDetails", this.GetKepModel(id));
        }
        
        // GET: Kep/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedKep = GetKepModel(id);
            return View("KepIndex", this.vm);
        }

        // POST: Kep/Edit/5
        [HttpPost]
        public ActionResult Edit(Models.Kep kep, string editAction)
        {
            if(ModelState.IsValid && kep != null)
            {
                if(editAction == "AddNew")
                {
                    // A logic add nem adatokat vár, hanem már kész példányt.
                    // A MapperFactory-ban megírt reverseMap alapján viszont vissza tudunk nyerni egy ilyen példányt
                    Data.Kep dKep = mapper.Map<Models.Kep, Data.Kep>(kep);

                    logic.AddKep(dKep);
                }
                else
                {
                    Data.Kep dKep = mapper.Map<Models.Kep, Data.Kep>(kep);
                    bool success = this.logic.UpdateKep((int)dKep.kep_id, dKep.rendelesi_kod, dKep.eleresi_ut, dKep.fotos, dKep.tajolas ?? 0);
                    if (!success)
                        TempData["editResult"] = "Edit fail";
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedKep = kep;
                return View("KepIndex", this.vm);
            }
        }

        // GET: Kep/Delete/5
        public ActionResult Delete(int id)
        {
            // A törlés nem bool-t ad vissza, hanem kivételt dob a logicban.
            try
            {
                this.logic.DeleteKep(this.logic.GetKepek().FirstOrDefault(k => k.kep_id == id));
                TempData["editResult"] = "Sikeres törlés";
            }
            catch
            {
                TempData["editResult"] = "Sikertelen törlés";
            }
            
            return RedirectToAction(nameof(Index));
        }
    }
}
