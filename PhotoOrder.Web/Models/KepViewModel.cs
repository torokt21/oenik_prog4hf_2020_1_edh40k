﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhotoOrder.Web.Models
{
    public class KepViewModel
    {
        public Kep EditedKep { get; set; }

        public List<Kep> Kepek { get; set; }
    }
}