﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhotoOrder.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PhotoOrder.Data.Kep, PhotoOrder.Web.Models.Kep>()
                    .ForMember(dest => dest.Id, map => map.MapFrom(src => src.kep_id))
                    .ForMember(dest => dest.RendelesiKod, map => map.MapFrom(src => src.rendelesi_kod))
                    .ForMember(dest => dest.Fotos, map => map.MapFrom(src => src.fotos))
                    .ForMember(dest => dest.EleresiUt, map => map.MapFrom(src => src.eleresi_ut))
                    .ForMember(dest => dest.Tajolas, map => map.MapFrom(src => src.tajolas == 0 ? "Fekvő" : "Álló"))
                .ReverseMap()
                    .ForMember(dest => dest.kep_id, map => map.MapFrom(src => src.Id))
                    .ForMember(dest => dest.rendelesi_kod, map => map.MapFrom(src => src.RendelesiKod))
                    .ForMember(dest => dest.fotos, map => map.MapFrom(src => src.Fotos))
                    .ForMember(dest => dest.eleresi_ut, map => map.MapFrom(src => src.EleresiUt))
                    .ForMember(dest => dest.tajolas, map => map.MapFrom(src => src.Tajolas.ToLower() == "fekvő" ? 0 : 1));
            }
            );

            return config.CreateMapper();
        }
    }
}