﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhotoOrder.Web.Models
{
    public class Kep
    {
        [Display(Name = "Kép azonosító")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Rendelési kód")]
        [Required]
        [StringLength(10)]
        public string RendelesiKod { get; set; }

        [Display(Name = "Fotós")]
        [Required]
        public string Fotos { get; set; }

        [Display(Name = "Elérési út")]
        [Required]
        [StringLength(128, MinimumLength = 4)]
        public string EleresiUt { get; set; }

        [Display(Name = "Tájolás")]
        [Required]
        public string Tajolas { get; set; }
    }
}