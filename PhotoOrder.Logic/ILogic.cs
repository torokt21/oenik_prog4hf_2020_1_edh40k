﻿// <copyright file="ILogic.cs" company="TThread">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using PhotoOrder.Data;

    /// <summary>
    /// Az üzleti logika feladatait megkövetelő interface.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Visszaadja az összes rendelés gyűjteményét.
        /// </summary>
        /// <returns>A rendelések.</returns>
        IQueryable<Rendeles> GetRendelesek();

        /// <summary>
        /// Új rendelést ad a gyűjteményhez.
        /// </summary>
        /// <param name="ujRendeles">Az új rendelés példánya.</param>
        void AddRendeles(Rendeles ujRendeles);

        /// <summary>
        /// Töröl egy megadott rendelést.
        /// </summary>
        /// <param name="torlendo">A törlendő rendelés.</param>
        void DeleteRendeles(Rendeles torlendo);

        /// <summary>
        /// Felülírja egy rendelés mennyiségét.
        /// </summary>
        /// <param name="rendelesId">A rendelés azonosítója.</param>
        /// <param name="mennyiseg">A rendelés új mennyisége.</param>
        void UpdateRendelesDarabszam(int rendelesId, int mennyiseg);

        /// <summary>
        /// Módosítja a megadott képet
        /// </summary>
        /// <param name="id">A kép id-je.</param>
        /// <param name="rendelesiKod">A rendelési kód.</param>
        /// <param name="eleresiUt">A kép eléséri útja.</param>
        /// <param name="fotos">A fotós aki ak épet készítette.</param>
        /// <param name="tajolas">A kép tájolása.</param>
        /// <returns>Igaz, ha a módosítás sikeres.</returns>
        bool UpdateKep(int id, string rendelesiKod, string eleresiUt, string fotos, decimal tajolas);

        /// <summary>
        /// Visszaadja az összes szolgáltatás gyűjteményét.
        /// </summary>
        /// <returns>A rendelések.</returns>
        IQueryable<Szolgaltatas> GetSzolgaltatasok();

        /// <summary>
        /// Új szolgáltatást ad a gyűjteményhez.
        /// </summary>
        /// <param name="ujSSzolgaltatas">Az új szolgáltatás példánya.</param>
        void AddSzolgaltatas(Szolgaltatas ujSSzolgaltatas);

        /// <summary>
        /// Töröl egy megadott szolgáltatást.
        /// </summary>
        /// <param name="torlendo">A törlendő szolgáltatás.</param>
        void DeleteSzolgaltatas(Szolgaltatas torlendo);

        /// <summary>
        /// Visszaadja az összes kép gyűjteményét.
        /// </summary>
        /// <returns>A képek.</returns>
        IQueryable<Kep> GetKepek();

        /// <summary>
        /// Hozzáad egy képet a gyűjteményhez.
        /// </summary>
        /// <param name="ujKep">Az új kép.</param>
        void AddKep(Kep ujKep);

        /// <summary>
        /// Töröl egy képet a gyűjteményből.
        /// </summary>
        /// <param name="kep">A törlendő kép.</param>
        void DeleteKep(Kep kep);

        /// <summary>
        /// Visszaadja az összes kép gyűjteményét.
        /// </summary>
        /// <returns>A képek.</returns>
        IQueryable<Rendeles_info> GetRendelesInfok();

        /// <summary>
        /// Hozzáad egy rendelés infót a gyűjteményhez.
        /// </summary>
        /// <param name="ujRendelesInfo">Az új rendelés infó.</param>
        void AddRendelesInfo(Rendeles_info ujRendelesInfo);

        /// <summary>
        /// Módosítja a megadott rendelésinfó osztályát.
        /// </summary>
        /// <param name="rendelesInfo_id">A rendelés infó.</param>
        /// <param name="osztaly">Az új osztály.</param>
        void UpdateRendelesInfoOsztaly(int rendelesInfo_id, string osztaly);

        /// <summary>
        /// Töröl egy rendelés infót a gyűjteményből.
        /// </summary>
        /// <param name="rendelesInfo">A törlendő rendlés infó.</param>
        void DeleteRendelesInfo(Rendeles_info rendelesInfo);

        /// <summary>
        /// Módosítja a szolgáltatás egységárát.
        /// </summary>
        /// <param name="szolgaltatas_id">Módosítandó szolgáltatás.</param>
        /// <param name="price">Az új ár.</param>
        void UpdateSzolgaltatasEgysegar(int szolgaltatas_id, int price);

        /// <summary>
        /// Elforgatja a megadott képet.
        /// </summary>
        /// <param name="kep">Az elforgatandó kép.</param>
        void KepElforgat(int kep);

        /// <summary>
        /// Visszaadja a megadott fotós által készített képeket.
        /// </summary>
        /// <param name="fotos">A keresendő fotós.</param>
        /// <returns>A fotós által készített képek.</returns>
        IEnumerable<Kep> GetKepekByFotos(string fotos);

        /// <summary>
        /// Megadja az adott azonosítójú rendelés összárát.
        /// </summary>
        /// <param name="rendelesId">A rendelés azonosítója.</param>
        /// <returns>Az összesített fizetendő ár.</returns>
        int GetRendelesOsszar(int rendelesId);

        /// <summary>
        /// Kiszámolja az összes leadott rendelés átlagos árát.
        /// </summary>
        /// <returns>Az összes leadott rendelés átlagos ára.</returns>
        double GetRendelesekAtlaga();

        /// <summary>
        /// Megadja, hogy összesen hány darabot rendeltek az egyes szolgáltatásokból.
        /// </summary>
        /// <returns>Szolgáltatás-darabszám páros.</returns>
        IEnumerable<SzolgaltasasDarabItem> GetDarabszamokBySzolgaltatas();
    }
}
