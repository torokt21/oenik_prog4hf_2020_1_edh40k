﻿// <copyright file="Arajanlat.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Logic
{
    using Newtonsoft.Json;

    /// <summary>
    /// Az API által visszaadott árajánlat szerkezete.
    /// </summary>
    public class Arajanlat
    {
        /// <summary>
        /// Gets or sets vezetéknév.
        /// </summary>
        [JsonProperty("vezeteknev")]
        public string Vezeteknev { get; set; }

        /// <summary>
        /// Gets or sets keresznev.
        /// </summary>
        [JsonProperty("keresztnev")]
        public string Keresztnev { get; set; }

        /// <summary>
        /// Gets or sets osztaly.
        /// </summary>
        [JsonProperty("osztaly")]
        public string Osztaly { get; set; }

        /// <summary>
        /// Gets or sets összár.
        /// </summary>
        [JsonProperty("osszar")]
        public long Osszar { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format("{0} {1} ({2}) a következő árajánlatot kapta: {3}", this.Vezeteknev, this.Keresztnev, this.Osztaly, this.Osszar);
        }
    }
}
