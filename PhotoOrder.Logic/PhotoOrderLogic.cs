﻿// <copyright file="PhotoOrderLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using PhotoOrder.Data;
    using PhotoOrder.Repository;

    /// <summary>
    /// A szolgáltatások kezelését irányító üzleti logika.
    /// </summary>
    public class PhotoOrderLogic : ILogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PhotoOrderLogic"/> class.
        /// </summary>
        /// <param name="context">A DbContext.</param>
        public PhotoOrderLogic(DbContext context)
        {
            this.SzolgaltatasRepository = new SzolgaltatasRepository(context);
            this.KepRepository = new KepRepository(context);
            this.RendelesInfoRepository = new RendelesInfoRepository(context);
            this.RendelesRepository = new RendelesRepository(context);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PhotoOrderLogic"/> class.
        /// </summary>
        public PhotoOrderLogic()
        {
        }

        /// <inheritdoc/>
        public bool UpdateKep(int id, string rendelesiKod, string eleresiUt, string fotos, decimal tajolas)
        {
            return this.KepRepository.EditKep(id, rendelesiKod, eleresiUt, fotos, tajolas);
        }

        /// <summary>
        /// Gets or sets a rendelés repo.
        /// </summary>
        public IRendelesRepository RendelesRepository { get; set; }

        /// <summary>
        /// Gets or sets a szolgáltatás repo.
        /// </summary>
        public ISzolgaltatasRepository SzolgaltatasRepository { get; set; }

        /// <summary>
        /// Gets or sets a rendelés infó repo.
        /// </summary>
        public IRendelesInfoRepository RendelesInfoRepository { get; set; }

        /// <summary>
        /// Gets or sets a rendelés infó repo.
        /// </summary>
        public IKepRepository KepRepository { get; set; }

        /// <inheritdoc/>
        public void AddRendeles(Rendeles ujRendeles)
        {
            this.RendelesRepository.Insert(ujRendeles);
        }

        /// <inheritdoc/>
        public void AddSzolgaltatas(Szolgaltatas ujSSzolgaltatas)
        {
            this.SzolgaltatasRepository.Insert(ujSSzolgaltatas);
        }

        /// <inheritdoc/>
        public void DeleteRendeles(Rendeles torlendo)
        {
            this.RendelesRepository.Remove(torlendo);
        }

        /// <inheritdoc/>
        public IQueryable<Rendeles> GetRendelesek()
        {
            return this.RendelesRepository.GetAll();
        }

        /// <inheritdoc/>
        public void DeleteSzolgaltatas(Szolgaltatas torlendo)
        {
            this.SzolgaltatasRepository.Remove(torlendo);
        }

        /// <inheritdoc/>
        public IQueryable<Szolgaltatas> GetSzolgaltatasok()
        {
            return this.SzolgaltatasRepository.GetAll();
        }

        /// <inheritdoc/>
        public void UpdateRendelesDarabszam(int rendelesId, int mennyiseg)
        {
            if (mennyiseg < 1)
            {
                throw new ArgumentException("Hibás mennyiség!");
            }

            this.RendelesRepository.UpdateMennyiseg(rendelesId, mennyiseg);
        }

        /// <inheritdoc/>
        public IQueryable<Kep> GetKepek()
        {
            return this.KepRepository.GetAll();
        }

        /// <inheritdoc/>
        public void AddKep(Kep ujKep)
        {
            this.KepRepository.Insert(ujKep);
        }

        /// <inheritdoc/>
        public void DeleteKep(Kep kep)
        {
            this.KepRepository.Remove(kep);
        }

        /// <inheritdoc/>
        public IQueryable<Rendeles_info> GetRendelesInfok()
        {
            return this.RendelesInfoRepository.GetAll();
        }

        /// <inheritdoc/>
        public void AddRendelesInfo(Rendeles_info ujRendelesInfo)
        {
            this.RendelesInfoRepository.Insert(ujRendelesInfo);
        }

        /// <inheritdoc/>
        public void DeleteRendelesInfo(Rendeles_info rendelesInfo)
        {
            this.RendelesInfoRepository.Remove(rendelesInfo);
        }

        /// <inheritdoc/>
        public void UpdateSzolgaltatasEgysegar(int szolgaltatas_id, int price)
        {
            this.SzolgaltatasRepository.UpdateEgysegar(szolgaltatas_id, price);
        }

        /// <inheritdoc/>
        public void KepElforgat(int kep)
        {
           this.KepRepository.Elforgat(kep);
        }

        /// <inheritdoc/>
        public IEnumerable<Kep> GetKepekByFotos(string fotos)
        {
            return this.KepRepository.GetAll().Where(x => x.fotos == fotos);
        }

        /// <inheritdoc/>
        public void UpdateRendelesInfoOsztaly(int rendelesInfo, string osztaly)
        {
            this.RendelesInfoRepository.UpdateOsztaly(rendelesInfo, osztaly);
        }

        /// <inheritdoc/>
        public int GetRendelesOsszar(int rendelesId)
        {
            IQueryable<Rendeles> rendelesek =
                this.RendelesRepository.GetAll()
                    .Where(r => r.rendeles_id == rendelesId);

            if (rendelesek.Count() < 1)
            {
                throw new KeyNotFoundException();
            }

            return (int)rendelesek
                .Sum(r => r.Szolgaltatas.egysegar * r.darabszam)
                .Value;
        }

        /// <inheritdoc/>
        public double GetRendelesekAtlaga()
        {
            return (double)this.RendelesInfoRepository.GetAll()
                .Select(x => x.Rendeles.Sum(y => y.darabszam * y.Szolgaltatas.egysegar))
                .Average();
        }

        /// <inheritdoc/>
        public IEnumerable<SzolgaltasasDarabItem> GetDarabszamokBySzolgaltatas()
        {
            return this.RendelesRepository.GetAll()
                .GroupBy(x => x.Szolgaltatas)
                .Select(x => new SzolgaltasasDarabItem()
                {
                    Szolgaltatas = x.Key, Darab = x.Sum(s => (int)s.darabszam),
                });
        }
    }
}
