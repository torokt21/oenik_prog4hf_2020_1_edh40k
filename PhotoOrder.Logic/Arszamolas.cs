﻿// <copyright file="Arszamolas.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Logic
{
    using System.Linq;
    using System.Net;
    using System.Text;
    using Newtonsoft.Json;
    using PhotoOrder.Data;

    /// <summary>
    /// Az árajánlat lekérését vezérlő osztály. Működése során a külső Java endpointot használja.
    /// </summary>
    public class Arszamolas
    {
        private const string JavaEndpoint = "http://localhost:8080/arszamolas";

        /// <summary>
        /// Felkeresi a java endpointot, és kér egy árajánlatot a megadott rendelésre.
        /// </summary>
        /// <param name="rendelesInfo">A rendelés, amelyre az árajánlatot kérjük.</param>
        /// <returns>A válaszként kaéott árajánlat objektum.</returns>
        public static Arajanlat GetArajanlat(Rendeles_info rendelesInfo)
        {
            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                client.QueryString.Add("vezeteknev", rendelesInfo.vezeteknev);
                client.QueryString.Add("keresztnev", rendelesInfo.keresztnev);
                client.QueryString.Add("osztaly", rendelesInfo.osztaly);
                client.QueryString.Add("osszar", rendelesInfo.Rendeles.Sum(x => x.Szolgaltatas.egysegar).ToString());

                string result = client.DownloadString(JavaEndpoint);
                return JsonConvert.DeserializeObject<Arajanlat>(result);
            }
        }
    }
}
