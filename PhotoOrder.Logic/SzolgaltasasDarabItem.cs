﻿// <copyright file="SzolgaltasasDarabItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Logic
{
    using PhotoOrder.Data;

    /// <summary>
    /// A szolgáltatások szerinti lebontás eredményének típusa.
    /// </summary>
    public class SzolgaltasasDarabItem
    {
        /// <summary>
        /// Gets or sets a szolgáltatás.
        /// </summary>
        public Szolgaltatas Szolgaltatas { get; set; }

        /// <summary>
        /// Gets or sets a szolgáltatásból leadott rendelések száma.
        /// </summary>
        public int Darab { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is SzolgaltasasDarabItem)
            {
                SzolgaltasasDarabItem other = obj as SzolgaltasasDarabItem;
                return other.Szolgaltatas == this.Szolgaltatas && other.Darab == this.Darab;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Szolgaltatas.GetHashCode() + this.Darab.GetHashCode();
        }
    }
}
