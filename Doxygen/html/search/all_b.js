var searchData=
[
  ['remove_55',['Remove',['../interface_photo_order_1_1_repository_1_1_i_repository.html#a97a8c776fab184a2a449f70863c2fbf5',1,'PhotoOrder.Repository.IRepository.Remove()'],['../class_photo_order_1_1_repository_1_1_repository.html#af1d1ad0be3c10de4af7215755312f271',1,'PhotoOrder.Repository.Repository.Remove()']]],
  ['rendeles_56',['Rendeles',['../class_photo_order_1_1_data_1_1_rendeles.html',1,'PhotoOrder::Data']]],
  ['rendeles_5finfo_57',['Rendeles_info',['../class_photo_order_1_1_data_1_1_rendeles__info.html',1,'PhotoOrder::Data']]],
  ['rendelesinforepository_58',['RendelesInfoRepository',['../class_photo_order_1_1_repository_1_1_rendeles_info_repository.html',1,'PhotoOrder.Repository.RendelesInfoRepository'],['../class_photo_order_1_1_logic_1_1_photo_order_logic.html#afdaff0d0bc5b8a2cae7f23fbe8016bb8',1,'PhotoOrder.Logic.PhotoOrderLogic.RendelesInfoRepository()'],['../class_photo_order_1_1_repository_1_1_rendeles_info_repository.html#a981914cacb949b7ae9d5b702ffe4faea',1,'PhotoOrder.Repository.RendelesInfoRepository.RendelesInfoRepository()']]],
  ['rendelesinfotests_59',['RendelesInfoTests',['../class_photo_order_1_1_logic_1_1_tests_1_1_rendeles_info_tests.html',1,'PhotoOrder::Logic::Tests']]],
  ['rendelesrepository_60',['RendelesRepository',['../class_photo_order_1_1_repository_1_1_rendeles_repository.html',1,'PhotoOrder.Repository.RendelesRepository'],['../class_photo_order_1_1_logic_1_1_photo_order_logic.html#acb297e289d3edf22a2f394b70c7608ff',1,'PhotoOrder.Logic.PhotoOrderLogic.RendelesRepository()'],['../class_photo_order_1_1_repository_1_1_rendeles_repository.html#aa256a6435779de99e44f2a0e4e6b9e75',1,'PhotoOrder.Repository.RendelesRepository.RendelesRepository()']]],
  ['rendelestests_61',['RendelesTests',['../class_photo_order_1_1_logic_1_1_tests_1_1_rendeles_tests.html',1,'PhotoOrder::Logic::Tests']]],
  ['repository_62',['Repository',['../class_photo_order_1_1_repository_1_1_repository.html',1,'PhotoOrder.Repository.Repository&lt; T &gt;'],['../class_photo_order_1_1_repository_1_1_repository.html#ad66e0a604c62504c90a6e81720a8b751',1,'PhotoOrder.Repository.Repository.Repository()']]],
  ['repository_3c_20kep_20_3e_63',['Repository&lt; Kep &gt;',['../class_photo_order_1_1_repository_1_1_repository.html',1,'PhotoOrder::Repository']]],
  ['repository_3c_20rendeles_20_3e_64',['Repository&lt; Rendeles &gt;',['../class_photo_order_1_1_repository_1_1_repository.html',1,'PhotoOrder::Repository']]],
  ['repository_3c_20rendeles_5finfo_20_3e_65',['Repository&lt; Rendeles_info &gt;',['../class_photo_order_1_1_repository_1_1_repository.html',1,'PhotoOrder::Repository']]],
  ['repository_3c_20szolgaltatas_20_3e_66',['Repository&lt; Szolgaltatas &gt;',['../class_photo_order_1_1_repository_1_1_repository.html',1,'PhotoOrder::Repository']]]
];
