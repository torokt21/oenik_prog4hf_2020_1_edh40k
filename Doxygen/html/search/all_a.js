var searchData=
[
  ['data_47',['Data',['../namespace_photo_order_1_1_data.html',1,'PhotoOrder']]],
  ['logic_48',['Logic',['../namespace_photo_order_1_1_logic.html',1,'PhotoOrder']]],
  ['photoorder_49',['PhotoOrder',['../namespace_photo_order.html',1,'']]],
  ['photoorderlogic_50',['PhotoOrderLogic',['../class_photo_order_1_1_logic_1_1_photo_order_logic.html',1,'PhotoOrder.Logic.PhotoOrderLogic'],['../class_photo_order_1_1_logic_1_1_photo_order_logic.html#a4710b984dc27d86efb032a9912c666de',1,'PhotoOrder.Logic.PhotoOrderLogic.PhotoOrderLogic(DbContext context)'],['../class_photo_order_1_1_logic_1_1_photo_order_logic.html#a879157629cd87e21107a6d700e5a5754',1,'PhotoOrder.Logic.PhotoOrderLogic.PhotoOrderLogic()']]],
  ['photoordermodel_51',['PhotoOrderModel',['../class_photo_order_1_1_data_1_1_photo_order_model.html',1,'PhotoOrder::Data']]],
  ['program_52',['Program',['../class_photo_order_1_1_program_1_1_program.html',1,'PhotoOrder.Program.Program'],['../namespace_photo_order_1_1_program.html',1,'PhotoOrder.Program']]],
  ['repository_53',['Repository',['../namespace_photo_order_1_1_repository.html',1,'PhotoOrder']]],
  ['tests_54',['Tests',['../namespace_photo_order_1_1_logic_1_1_tests.html',1,'PhotoOrder::Logic']]]
];
