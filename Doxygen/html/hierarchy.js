var hierarchy =
[
    [ "PhotoOrder.Logic.Arajanlat", "class_photo_order_1_1_logic_1_1_arajanlat.html", null ],
    [ "PhotoOrder.Logic.Arszamolas", "class_photo_order_1_1_logic_1_1_arszamolas.html", null ],
    [ "DbContext", null, [
      [ "PhotoOrder.Data.PhotoOrderModel", "class_photo_order_1_1_data_1_1_photo_order_model.html", null ]
    ] ],
    [ "PhotoOrder.Logic.ILogic", "interface_photo_order_1_1_logic_1_1_i_logic.html", [
      [ "PhotoOrder.Logic.PhotoOrderLogic", "class_photo_order_1_1_logic_1_1_photo_order_logic.html", null ]
    ] ],
    [ "PhotoOrder.Repository.IRepository< T >", "interface_photo_order_1_1_repository_1_1_i_repository.html", [
      [ "PhotoOrder.Repository.Repository< T >", "class_photo_order_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "PhotoOrder.Repository.IRepository< Kep >", "interface_photo_order_1_1_repository_1_1_i_repository.html", [
      [ "PhotoOrder.Repository.IKepRepository", "interface_photo_order_1_1_repository_1_1_i_kep_repository.html", [
        [ "PhotoOrder.Repository.KepRepository", "class_photo_order_1_1_repository_1_1_kep_repository.html", null ]
      ] ]
    ] ],
    [ "PhotoOrder.Repository.IRepository< Rendeles >", "interface_photo_order_1_1_repository_1_1_i_repository.html", [
      [ "PhotoOrder.Repository.IRendelesRepository", "interface_photo_order_1_1_repository_1_1_i_rendeles_repository.html", [
        [ "PhotoOrder.Repository.RendelesRepository", "class_photo_order_1_1_repository_1_1_rendeles_repository.html", null ]
      ] ]
    ] ],
    [ "PhotoOrder.Repository.IRepository< Rendeles_info >", "interface_photo_order_1_1_repository_1_1_i_repository.html", [
      [ "PhotoOrder.Repository.IRendelesInfoRepository", "interface_photo_order_1_1_repository_1_1_i_rendeles_info_repository.html", [
        [ "PhotoOrder.Repository.RendelesInfoRepository", "class_photo_order_1_1_repository_1_1_rendeles_info_repository.html", null ]
      ] ]
    ] ],
    [ "PhotoOrder.Repository.IRepository< Szolgaltatas >", "interface_photo_order_1_1_repository_1_1_i_repository.html", [
      [ "PhotoOrder.Repository.ISzolgaltatasRepository", "interface_photo_order_1_1_repository_1_1_i_szolgaltatas_repository.html", [
        [ "PhotoOrder.Repository.SzolgaltatasRepository", "class_photo_order_1_1_repository_1_1_szolgaltatas_repository.html", null ]
      ] ]
    ] ],
    [ "PhotoOrder.Data.Kep", "class_photo_order_1_1_data_1_1_kep.html", null ],
    [ "PhotoOrder.Logic.Tests.KepTests", "class_photo_order_1_1_logic_1_1_tests_1_1_kep_tests.html", null ],
    [ "PhotoOrder.Logic.Tests.LogicTests", "class_photo_order_1_1_logic_1_1_tests_1_1_logic_tests.html", null ],
    [ "PhotoOrder.Program.MenuRendszer", "class_photo_order_1_1_program_1_1_menu_rendszer.html", null ],
    [ "PhotoOrder.Program.Program", "class_photo_order_1_1_program_1_1_program.html", null ],
    [ "PhotoOrder.Data.Rendeles", "class_photo_order_1_1_data_1_1_rendeles.html", null ],
    [ "PhotoOrder.Data.Rendeles_info", "class_photo_order_1_1_data_1_1_rendeles__info.html", null ],
    [ "PhotoOrder.Logic.Tests.RendelesInfoTests", "class_photo_order_1_1_logic_1_1_tests_1_1_rendeles_info_tests.html", null ],
    [ "PhotoOrder.Logic.Tests.RendelesTests", "class_photo_order_1_1_logic_1_1_tests_1_1_rendeles_tests.html", null ],
    [ "PhotoOrder.Repository.Repository< Kep >", "class_photo_order_1_1_repository_1_1_repository.html", [
      [ "PhotoOrder.Repository.KepRepository", "class_photo_order_1_1_repository_1_1_kep_repository.html", null ]
    ] ],
    [ "PhotoOrder.Repository.Repository< Rendeles >", "class_photo_order_1_1_repository_1_1_repository.html", [
      [ "PhotoOrder.Repository.RendelesRepository", "class_photo_order_1_1_repository_1_1_rendeles_repository.html", null ]
    ] ],
    [ "PhotoOrder.Repository.Repository< Rendeles_info >", "class_photo_order_1_1_repository_1_1_repository.html", [
      [ "PhotoOrder.Repository.RendelesInfoRepository", "class_photo_order_1_1_repository_1_1_rendeles_info_repository.html", null ]
    ] ],
    [ "PhotoOrder.Repository.Repository< Szolgaltatas >", "class_photo_order_1_1_repository_1_1_repository.html", [
      [ "PhotoOrder.Repository.SzolgaltatasRepository", "class_photo_order_1_1_repository_1_1_szolgaltatas_repository.html", null ]
    ] ],
    [ "PhotoOrder.Logic.SzolgaltasasDarabItem", "class_photo_order_1_1_logic_1_1_szolgaltasas_darab_item.html", null ],
    [ "PhotoOrder.Data.Szolgaltatas", "class_photo_order_1_1_data_1_1_szolgaltatas.html", null ],
    [ "PhotoOrder.Logic.Tests.SzolgaltatasTests", "class_photo_order_1_1_logic_1_1_tests_1_1_szolgaltatas_tests.html", null ]
];