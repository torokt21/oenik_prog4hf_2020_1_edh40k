var interface_photo_order_1_1_logic_1_1_i_logic =
[
    [ "AddKep", "interface_photo_order_1_1_logic_1_1_i_logic.html#a6e002c7a57153da4ecaef235ce840149", null ],
    [ "AddRendeles", "interface_photo_order_1_1_logic_1_1_i_logic.html#a1b6124cf96db018c66715ac3b827b0ce", null ],
    [ "AddRendelesInfo", "interface_photo_order_1_1_logic_1_1_i_logic.html#a1edc48213d3f046436cf5c0756e0a870", null ],
    [ "AddSzolgaltatas", "interface_photo_order_1_1_logic_1_1_i_logic.html#ade1c76f0a1afb58609ee3895948e2ab8", null ],
    [ "DeleteKep", "interface_photo_order_1_1_logic_1_1_i_logic.html#a123930c7c49f5e6dc5df1fc366f7d45c", null ],
    [ "DeleteRendeles", "interface_photo_order_1_1_logic_1_1_i_logic.html#acb9bb3083a75b9deafbd920f8c88ad78", null ],
    [ "DeleteRendelesInfo", "interface_photo_order_1_1_logic_1_1_i_logic.html#a49002979f22ff48f2b5765070e49d65e", null ],
    [ "DeleteSzolgaltatas", "interface_photo_order_1_1_logic_1_1_i_logic.html#a6d20b6eeb31ba242d11aed0e6ac39966", null ],
    [ "GetDarabszamokBySzolgaltatas", "interface_photo_order_1_1_logic_1_1_i_logic.html#a624ad185f7e82384c3e4e488048d2797", null ],
    [ "GetKepek", "interface_photo_order_1_1_logic_1_1_i_logic.html#afc41e8775a72ca1a2884827baca47278", null ],
    [ "GetKepekByFotos", "interface_photo_order_1_1_logic_1_1_i_logic.html#a0836f6de44c5f75aa5fa96be64c75509", null ],
    [ "GetRendelesek", "interface_photo_order_1_1_logic_1_1_i_logic.html#adfbd89bac5384754373ee2bf458769d1", null ],
    [ "GetRendelesekAtlaga", "interface_photo_order_1_1_logic_1_1_i_logic.html#a6de9591e50015e032c766d716b9b1efb", null ],
    [ "GetRendelesInfok", "interface_photo_order_1_1_logic_1_1_i_logic.html#a491b20033c68eb34c25f521f7c446a27", null ],
    [ "GetRendelesOsszar", "interface_photo_order_1_1_logic_1_1_i_logic.html#aa73c23f926db5fdf763cb7a651d06e60", null ],
    [ "GetSzolgaltatasok", "interface_photo_order_1_1_logic_1_1_i_logic.html#ac13579efdbb02fc75aec49b4f6e50c77", null ],
    [ "KepElforgat", "interface_photo_order_1_1_logic_1_1_i_logic.html#ae17bf7159dfa2d947a2f9e46e083856d", null ],
    [ "UpdateRendelesDarabszam", "interface_photo_order_1_1_logic_1_1_i_logic.html#afe072eaf949c06e7e25972ce97700b52", null ],
    [ "UpdateRendelesInfoOsztaly", "interface_photo_order_1_1_logic_1_1_i_logic.html#a03dc76e068d4c1f22f94d6c1e84bf15b", null ],
    [ "UpdateSzolgaltatasEgysegar", "interface_photo_order_1_1_logic_1_1_i_logic.html#ac50868a1f9fc49050469381f95fd2be5", null ]
];