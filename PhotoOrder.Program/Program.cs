﻿// <copyright file="Program.cs" company="TThread">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Program
{
    using System;

    /// <summary>
    /// A konzolos alkalmazás fő osztálya.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// A program belépési pontja.
        /// </summary>
        /// <param name="args">A program parancssori argumentumai.</param>
        public static void Main(string[] args)
        {
            MenuRendszer menu = new MenuRendszer();

            menu.MainMenu();

            Console.ReadKey();
        }
    }
}
