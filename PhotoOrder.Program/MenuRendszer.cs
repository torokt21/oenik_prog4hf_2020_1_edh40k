﻿// <copyright file="MenuRendszer.cs" company="TThread">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using PhotoOrder.Data;
    using PhotoOrder.Logic;

    /// <summary>
    /// A konzolra kiírást segítő class.
    /// </summary>
    public class MenuRendszer
    {
        private PhotoOrderLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuRendszer"/> class.
        /// </summary>
        public MenuRendszer()
        {
            this.logic = new PhotoOrderLogic(new PhotoOrderModel());
        }

        /// <summary>
        /// Megjeleníti a főmenüt, ahol a felhasználó eldönit mit szeretne csinálni.
        /// </summary>
        public void MainMenu()
        {
            int choice = -1;

            do
            {
                choice = ShowMenu(
                    "Mit szeretnél csinálni?",
                    "Képek kezelése",
                    "Szolgáltatások kezelése",
                    "Rendelések kezelése",
                    "Rendelések információinak kezelése",
                    "Egy rendelés árának lekérdezése",
                    "A rendelések átlagának lekérdezése",
                    "Rendelések száma szolgáltatásonként",
                    "Egy rendelés teljes árának lekérése (JAVA endpoint)");

                switch (choice)
                {
                    case 0:
                        this.CrudMenu<Kep>();
                        break;
                    case 1:
                        this.CrudMenu<Szolgaltatas>();
                        break;
                    case 2:
                        this.CrudMenu<Rendeles>();
                        break;
                    case 3:
                        this.CrudMenu<Rendeles_info>();
                        break;
                    case 4:
                        Console.WriteLine("Melyik rendelés összárát szeretnéd lekérni? (rendelés id)");
                        int id = int.Parse(Console.ReadLine());
                        Console.WriteLine(this.logic.GetRendelesOsszar(id));
                        Console.ReadKey();
                        break;
                    case 5:
                        Console.WriteLine("Rendelések átlaga: " + this.logic.GetRendelesekAtlaga());
                        Console.ReadKey();
                        break;
                    case 6:
                        this.logic.GetDarabszamokBySzolgaltatas()
                            .ToList()
                            .ForEach(x =>
                                Console.WriteLine("{0}: {1} db", x.Szolgaltatas.nev, x.Darab));
                        Console.ReadKey();
                        break;
                    case 7:
                        Console.WriteLine("Árajánlat kérés." + Environment.NewLine);
                        Console.WriteLine("Melyik rendelésre szeretnél ajánlatot kérni?");

                        int rid = int.Parse(Console.ReadLine());

                        Rendeles_info rend = this.logic.GetRendelesInfok().FirstOrDefault(x => x.rendeles_id == rid);

                        if (rend == null)
                        {
                            Console.WriteLine("Rossz id");
                            Console.ReadKey();
                            break;
                        }

                        Arajanlat ajanlat = Logic.Arszamolas.GetArajanlat(rend);

                        Console.WriteLine(ajanlat.ToString());
                        Console.ReadKey();
                        break;
                }
            }
            while (choice != -1);
        }

        /// <summary>
        /// Létrehoz egy új példányt a megadott típusbúl, és bekéri a tulajdonságait (string/decimal).
        /// </summary>
        /// <typeparam name="T">A létrehozandó típus.</typeparam>
        /// <returns>Új T típusú példány.</returns>
        private static T CreateObject<T>()
            where T : class, new()
        {
            Console.Clear();
            Console.WriteLine("Az új elem tulajdonságai:");

            // Tulajdonságok megszerzése
            var props = typeof(T).GetProperties();

            // Új példány
            T newObject = new T();

            foreach (PropertyInfo propertyInfo in props)
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    Console.WriteLine(propertyInfo.Name + ": ");
                    propertyInfo.SetValue(newObject, Console.ReadLine());
                }

                if (propertyInfo.PropertyType == typeof(decimal?) || propertyInfo.PropertyType == typeof(decimal))
                {
                    Console.WriteLine(propertyInfo.Name + ": ");
                    decimal value = decimal.Parse(Console.ReadLine());
                    propertyInfo.SetValue(newObject, value);
                }
            }

            return newObject;
        }

        /// <summary>
        /// Kiír egy egyszerű menürendszert a Console-ra.
        /// </summary>
        /// <param name="kerdes">A kérdést amit feltesz a menürendszer kirajzolása előtt.</param>
        /// <param name="menuPontok">A menüpontok tömbje.</param>
        /// <returns>A kiválaszott elem indexe.</returns>
        private static int ShowMenu(string kerdes, params string[] menuPontok)
        {
            int valasztas = -1;
            do
            {
                Console.Clear();

                Console.WriteLine(kerdes);

                for (int i = 0; i < menuPontok.Length; i++)
                {
                    Console.WriteLine((i + 1) + ". " + menuPontok[i]);
                }

                Console.WriteLine((menuPontok.Length + 1) + ". Kilépés");

                try
                {
                    valasztas = Convert.ToInt32(Console.ReadLine()) - 1;
                }
                catch (FormatException)
                {
                    valasztas = -1;
                }
            }
            while (valasztas < 0 || valasztas > menuPontok.Length);

            Console.Clear();
            return valasztas == menuPontok.Length ? -1 : valasztas;
        }

        /// <summary>
        /// Kirajzol egy menüt a konzolra, ami bekéri hogy melyik CRUD művelet a következő.
        /// </summary>
        /// <typeparam name="T">Amelyik típus CRUD menüje.</typeparam>
        private void CrudMenu<T>()
            where T : class, new()
        {
            int choice = ShowMenu(
                "Mit szeretnél csinálni?",
                "Új elem",
                "Elemek listázása",
                "Elem módosítása",
                "Elem törlése");

            // Create
            if (choice == 0)
            {
                T newObject = CreateObject<T>();
                if (typeof(T) == typeof(Rendeles))
                {
                    this.logic.AddRendeles(newObject as Rendeles);
                }
                else if (typeof(T) == typeof(Rendeles_info))
                {
                    this.logic.AddRendelesInfo(newObject as Rendeles_info);
                }
                else if (typeof(T) == typeof(Kep))
                {
                    this.logic.AddKep(newObject as Kep);
                }
                else if (typeof(T) == typeof(Szolgaltatas))
                {
                    this.logic.AddSzolgaltatas(newObject as Szolgaltatas);
                }
            }

            // Read
            else if (choice == 1)
            {
                if (typeof(T) == typeof(Rendeles))
                {
                    this.PrintTable(this.logic.GetRendelesek());
                }
                else if (typeof(T) == typeof(Rendeles_info))
                {
                    this.PrintTable(this.logic.GetRendelesInfok());
                }
                else if (typeof(T) == typeof(Kep))
                {
                    this.PrintTable(this.logic.GetKepek());
                }
                else if (typeof(T) == typeof(Szolgaltatas))
                {
                    this.PrintTable(this.logic.GetSzolgaltatasok());
                }
            }

            // Update
            else if (choice == 2)
            {
                if (typeof(T) == typeof(Rendeles))
                {
                    Console.WriteLine("Rendelés darabszámának módosítása");
                    Console.WriteLine("Rendelés id:");
                    int id = int.Parse(Console.ReadLine());

                    Console.WriteLine("Darabszám:");
                    int db = int.Parse(Console.ReadLine());

                    this.logic.UpdateRendelesDarabszam(id, db);
                }
                else if (typeof(T) == typeof(Rendeles_info))
                {
                    Console.WriteLine("Rendelés infó osztályának megváltoztatása");
                    Console.WriteLine("Rendelés infó id:");
                    int id = int.Parse(Console.ReadLine());

                    Console.WriteLine("Osztály:");
                    string osztaly = Console.ReadLine();

                    this.logic.UpdateRendelesInfoOsztaly(id, osztaly);
                }
                else if (typeof(T) == typeof(Kep))
                {
                    Console.WriteLine("Kép elforgatása");
                    Console.WriteLine("Kép id:");
                    int id = int.Parse(Console.ReadLine());

                    this.logic.KepElforgat(id);
                }
                else if (typeof(T) == typeof(Szolgaltatas))
                {
                    Console.WriteLine("Szolgáltatás árának módosítása");
                    Console.WriteLine("Szolgáltatás id:");
                    int id = int.Parse(Console.ReadLine());

                    Console.WriteLine("Új ár:");
                    Console.WriteLine("Darabszám:");
                    int price = int.Parse(Console.ReadLine());

                    this.logic.UpdateSzolgaltatasEgysegar(id, price);
                }
            }

            // Delete
            else if (choice == 3)
            {
                Console.WriteLine("Törlendő id:");
                int id = int.Parse(Console.ReadLine());

                if (typeof(T) == typeof(Rendeles))
                {
                    Rendeles rendeles = this.logic.GetRendelesek().FirstOrDefault(x => x.rendeles_elem_id == id);
                    if (rendeles != null)
                    {
                        this.logic.DeleteRendeles(rendeles);
                    }
                    else
                    {
                        Console.WriteLine("Hibás id!");
                    }
                }
                else if (typeof(T) == typeof(Rendeles_info))
                {
                    Rendeles_info rendelesInfo = this.logic.GetRendelesInfok().FirstOrDefault(x => x.rendeles_id == id);
                    if (rendelesInfo != null)
                    {
                        this.logic.DeleteRendelesInfo(rendelesInfo);
                    }
                    else
                    {
                        Console.WriteLine("Hibás id!");
                    }
                }
                else if (typeof(T) == typeof(Kep))
                {
                    Kep kep = this.logic.GetKepek().FirstOrDefault(x => x.kep_id == id);
                    if (kep != null)
                    {
                        this.logic.DeleteKep(kep);
                    }
                    else
                    {
                        Console.WriteLine("Hibás id!");
                    }
                }
                else if (typeof(T) == typeof(Szolgaltatas))
                {
                    Szolgaltatas szolgaltatas = this.logic.GetSzolgaltatasok().First(x => x.szolgaltatas_id == id);

                    if (szolgaltatas != null)
                    {
                        this.logic.DeleteSzolgaltatas(szolgaltatas);
                    }
                    else
                    {
                        Console.WriteLine("Hibás id!");
                    }
                }
            }

            Console.WriteLine("Nyomj meg egy gombot a továbblépéshez!");
            Console.ReadKey();
        }

        /// <summary>
        /// Kiír egy táblázatot a konzolra a megadott gyűjteményből.
        /// </summary>
        /// <typeparam name="T">A gyűjtemény típusa.</typeparam>
        /// <param name="collection">A gyűjtemény.</param>
        private void PrintTable<T>(IEnumerable<T> collection)
        {
            Console.Clear();
            PropertyInfo[] props = typeof(T).GetProperties();

            foreach (PropertyInfo propertyInfo in props)
            {
                if (propertyInfo.PropertyType.IsValueType || propertyInfo.PropertyType == typeof(string))
                {
                    Console.Write(propertyInfo.Name + "\t");
                }
            }

            Console.WriteLine();

            foreach (T row in collection)
            {
                foreach (PropertyInfo propertyInfo in props)
                {
                    if (propertyInfo.PropertyType.IsValueType || propertyInfo.PropertyType == typeof(string))
                    {
                        Console.Write(propertyInfo.GetValue(row) + "\t");
                    }
                }

                Console.WriteLine();
            }
        }
    }
}
