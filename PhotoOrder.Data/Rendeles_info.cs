namespace PhotoOrder.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Rendeles_info
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Rendeles_info()
        {
            Rendeles = new HashSet<Rendeles>();
        }

        [Key]
        public int rendeles_id { get; set; }

        [Required]
        [StringLength(24)]
        public string vezeteknev { get; set; }

        [Required]
        [StringLength(24)]
        public string keresztnev { get; set; }

        [StringLength(6)]
        public string osztaly { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rendeles> Rendeles { get; set; }
    }
}
