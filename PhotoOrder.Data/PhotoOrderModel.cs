namespace PhotoOrder.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PhotoOrderModel : DbContext
    {
        public PhotoOrderModel()
            : base("name=PhotoOrderModelConfig")
        {
        }

        public virtual DbSet<Kep> Kep { get; set; }
        public virtual DbSet<Rendeles> Rendeles { get; set; }
        public virtual DbSet<Rendeles_info> Rendeles_info { get; set; }
        public virtual DbSet<Szolgaltatas> Szolgaltatas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Kep>()
                .Property(e => e.rendelesi_kod)
                .IsUnicode(false);

            modelBuilder.Entity<Kep>()
                .Property(e => e.fotos)
                .IsUnicode(false);

            modelBuilder.Entity<Kep>()
                .Property(e => e.eleresi_ut)
                .IsUnicode(false);

            modelBuilder.Entity<Kep>()
                .Property(e => e.tajolas)
                .HasPrecision(1, 0);

            modelBuilder.Entity<Rendeles>()
                .Property(e => e.darabszam)
                .HasPrecision(8, 0);

            modelBuilder.Entity<Rendeles_info>()
                .Property(e => e.vezeteknev)
                .IsUnicode(false);

            modelBuilder.Entity<Rendeles_info>()
                .Property(e => e.keresztnev)
                .IsUnicode(false);

            modelBuilder.Entity<Rendeles_info>()
                .Property(e => e.osztaly)
                .IsUnicode(false);

            modelBuilder.Entity<Rendeles_info>()
                .HasMany(e => e.Rendeles)
                .WithOptional(e => e.Rendeles_info)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Szolgaltatas>()
                .Property(e => e.nev)
                .IsUnicode(false);

            modelBuilder.Entity<Szolgaltatas>()
                .Property(e => e.tipus)
                .IsUnicode(false);

            modelBuilder.Entity<Szolgaltatas>()
                .Property(e => e.egysegar)
                .HasPrecision(10, 0);
        }
    }
}
