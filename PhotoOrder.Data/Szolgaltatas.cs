namespace PhotoOrder.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Szolgaltatas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Szolgaltatas()
        {
            Rendeles = new HashSet<Rendeles>();
        }

        [Key]
        public int szolgaltatas_id { get; set; }

        [Required]
        [StringLength(50)]
        public string nev { get; set; }

        [Required]
        [StringLength(20)]
        public string tipus { get; set; }

        [Column(TypeName = "numeric")]
        public decimal egysegar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rendeles> Rendeles { get; set; }
    }
}
