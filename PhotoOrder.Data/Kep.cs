namespace PhotoOrder.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Kep")]
    public partial class Kep
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Kep()
        {
            Rendeles = new HashSet<Rendeles>();
        }

        [Key]
        public int kep_id { get; set; }

        [Required]
        [StringLength(12)]
        public string rendelesi_kod { get; set; }

        [StringLength(24)]
        public string fotos { get; set; }

        [StringLength(128)]
        public string eleresi_ut { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? tajolas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rendeles> Rendeles { get; set; }
    }
}
