﻿IF OBJECT_ID('Rendeles', 'U') IS NOT NULL DROP TABLE Rendeles;
IF OBJECT_ID('Kep', 'U') IS NOT NULL DROP TABLE Kep;
IF OBJECT_ID('Szolgaltatas', 'U') IS NOT NULL DROP TABLE Szolgaltatas;
IF OBJECT_ID('Rendeles_info', 'U') IS NOT NULL DROP TABLE Rendeles_info;


CREATE TABLE Kep
(
    kep_id NUMERIC(8) PRIMARY KEY,
    rendelesi_kod VARCHAR(12) NOT NULL,
    fotos VARCHAR(24),
    eleresi_ut VARCHAR(128),
    tajolas NUMERIC(1),
    CONSTRAINT tajol_chk CHECK (tajolas IN ('1', '0'))
);

CREATE TABLE Szolgaltatas
(
    szolgaltatas_id NUMERIC(8) PRIMARY KEY,
    nev VARCHAR(50) NOT NULL,
    tipus VARCHAR(20) NOT NULL,
    egysegar NUMERIC(10) NOT NULL,
    CONSTRAINT tipus_chk CHECK (tipus IN ('ajandektargy', 'papirkep'))
);

CREATE TABLE Rendeles_info
(
    rendeles_id NUMERIC(8) PRIMARY KEY,
    vezeteknev VARCHAR(24) NOT NULL,
    keresztnev VARCHAR(24) NOT NULL,
    osztaly VARCHAR(6)
);

CREATE TABLE Rendeles
(
	rendeles_elem_id NUMERIC(8) PRIMARY KEY,
    rendeles_id NUMERIC(8),
    szolgaltatas_id NUMERIC(8),
    kep_id NUMERIC(8),
    darabszam NUMERIC(8),
    CONSTRAINT db_min CHECK (darabszam > 0),
    CONSTRAINT rend_fk FOREIGN KEY (rendeles_id) REFERENCES Rendeles_info ON DELETE CASCADE,
    CONSTRAINT szolg_fk FOREIGN KEY (szolgaltatas_id) REFERENCES Szolgaltatas ON DELETE SET NULL,
    CONSTRAINT kep_fk FOREIGN KEY (kep_id) REFERENCES Kep ON DELETE SET NULL
);

INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('0','ABC1234567','Fényképész István','1','/gallery/0001.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('1','ABC1234567','Vörös Nándor','0','/gallery/0002.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('2','ABC1234567','Fényképész István','0','/gallery/0003.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('3','ABC1234567','Fényképész István','1','/gallery/0004.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('4','HEV1122334','Vörös Nándor','1','/gallery/0005.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('5','HEV1122334','Vörös Nándor','0','/gallery/0006.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('6','HEV1122334','Vörös Nándor','0','/gallery/0007.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('7','HEV1122334','Vörös Nándor','0','/gallery/0008.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('8','KFG1357924','Vörös Nándor','0','/gallery/0009.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('9','KFG1357924','Vörös Nándor','1','/gallery/0010.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('10','KFG1357924','Vörös Nándor','1','/gallery/0011.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('11','KFG1357924','Vörös Nándor','0','/gallery/0012.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('12','KFG1357924','Fényképész István','1','/gallery/0013.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('13','SZB4275971','Vörös Nándor','1','/gallery/0014.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('14','SZB4275971','Vörös Nándor','1','/gallery/0015.jpg');
INSERT INTO Kep (kep_id, rendelesi_kod, fotos, tajolas, eleresi_ut) VALUES ('15','SZB4275971','Fényképész István','1','/gallery/0016.jpg');


INSERT INTO Szolgaltatas (szolgaltatas_id, nev, tipus, egysegar) VALUES('1','9x13','papirkep','450');
INSERT INTO Szolgaltatas (szolgaltatas_id, nev, tipus, egysegar) VALUES('2','10x15','papirkep','500');
INSERT INTO Szolgaltatas (szolgaltatas_id, nev, tipus, egysegar) VALUES('3','13x19','papirkep','650');
INSERT INTO Szolgaltatas (szolgaltatas_id, nev, tipus, egysegar) VALUES('4','Igazolványkép','papirkep','850');
INSERT INTO Szolgaltatas (szolgaltatas_id, nev, tipus, egysegar) VALUES('5','Bögre','ajandektargy','2500');
INSERT INTO Szolgaltatas (szolgaltatas_id, nev, tipus, egysegar) VALUES('6','Hűtőmádnes','ajandektargy','1600');


INSERT INTO Rendeles_info (rendeles_id, vezeteknev, keresztnev, osztaly) VALUES('1','Török','Tamás','1.a');
INSERT INTO Rendeles_info (rendeles_id, vezeteknev, keresztnev, osztaly) VALUES('2','Kovács','Júlia','2.b');
INSERT INTO Rendeles_info (rendeles_id, vezeteknev, keresztnev, osztaly) VALUES('3','Bognár','Béla','1.a');
INSERT INTO Rendeles_info (rendeles_id, vezeteknev, keresztnev, osztaly) VALUES('4','Szent','István Jr.','3.b');
INSERT INTO Rendeles_info (rendeles_id, vezeteknev, keresztnev, osztaly) VALUES('5','Donald','Kacsa','5.a');


INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('1', '1', '2', '6', '2');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('2', '2', '3', '7', '4');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('3', '2', '1', '8', '3');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('4', '2', '1', '14', '4');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('5', '3', '4', '13', '3');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('6', '3', '6', '11', '4');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('7', '3', '6', '15', '4');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('8', '4', '2', '3', '3');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('9', '4', '2', '5', '1');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('10', '4', '3', '6', '4');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('11', '4', '6', '15', '1');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('12', '5', '4', '6', '3');
INSERT INTO Rendeles (rendeles_elem_id, rendeles_id, szolgaltatas_id, kep_id, darabszam) VALUES ('13', '5', '1', '9', '2');