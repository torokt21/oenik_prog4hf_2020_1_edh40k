namespace PhotoOrder.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Rendeles
    {
        [Key]
        public int rendeles_elem_id { get; set; }

        public int? rendeles_id { get; set; }

        public int? szolgaltatas_id { get; set; }

        public int? kep_id { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? darabszam { get; set; }

        public virtual Kep Kep { get; set; }

        public virtual Rendeles_info Rendeles_info { get; set; }

        public virtual Szolgaltatas Szolgaltatas { get; set; }
    }
}
