﻿// <copyright file="IRendelesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhotoOrder.Data;

    /// <summary>
    /// A rendeléseket kezelő repository interface.
    /// </summary>
    public interface IRendelesRepository : IRepository<Rendeles>
    {
        /// <summary>
        /// Egy leadott rendelés mennyiségét frissíti.
        /// </summary>
        /// <param name="rendeles_id">A módosítandó rendelés.</param>
        /// <param name="mennyiseg">A rendelés új mennyisége.</param>
        void UpdateMennyiseg(int rendeles_id, int mennyiseg);
    }
}
