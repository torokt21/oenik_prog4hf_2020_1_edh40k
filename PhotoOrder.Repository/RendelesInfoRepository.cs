﻿// <copyright file="RendelesInfoRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhotoOrder.Data;

    /// <summary>
    /// A rendelés infók repoja.
    /// </summary>
    public class RendelesInfoRepository : Repository<Rendeles_info>, IRendelesInfoRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RendelesInfoRepository"/> class.
        /// </summary>
        /// <param name="context">A context.</param>
        public RendelesInfoRepository(DbContext context)
            : base(context)
        {
        }

        /// <inheritdoc/>
        public void UpdateOsztaly(int rendeles_id, string osztaly)
        {
            Rendeles_info rinfo = this.GetAll().First(r => r.rendeles_id == rendeles_id);
            rinfo.osztaly = osztaly;
            this.Context.SaveChanges();
        }
    }
}
