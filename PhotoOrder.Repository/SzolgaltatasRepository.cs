﻿// <copyright file="SzolgaltatasRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhotoOrder.Data;

    /// <summary>
    /// Szolgáltatlás repository.
    /// </summary>
    public class SzolgaltatasRepository : Repository<Szolgaltatas>, ISzolgaltatasRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SzolgaltatasRepository"/> class.
        /// </summary>
        /// <param name="context">A context.</param>
        public SzolgaltatasRepository(DbContext context)
            : base(context)
        {
        }

        /// <inheritdoc/>
        public void UpdateEgysegar(int szolgaltatas_id, int egyszegar)
        {
            Szolgaltatas szolg = this.GetAll().First(sz => sz.szolgaltatas_id == szolgaltatas_id);
            szolg.egysegar = egyszegar;
            this.Context.SaveChanges();
        }
    }
}
