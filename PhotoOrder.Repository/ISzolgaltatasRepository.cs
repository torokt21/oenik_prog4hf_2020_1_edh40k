﻿// <copyright file="ISzolgaltatasRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhotoOrder.Data;

    /// <summary>
    /// A szolgáltatásokat kezelő repository.
    /// </summary>
    public interface ISzolgaltatasRepository : IRepository<Szolgaltatas>
    {
        /// <summary>
        /// Az egységár megváltoztatása.
        /// </summary>
        /// <param name="szolgaltatas_id">A szolgáltatás.</param>
        /// <param name="egyszegar">Az új egységár.</param>
        void UpdateEgysegar(int szolgaltatas_id, int egyszegar);
    }
}
