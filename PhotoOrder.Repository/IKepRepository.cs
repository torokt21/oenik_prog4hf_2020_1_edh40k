﻿// <copyright file="IKepRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhotoOrder.Data;

    /// <summary>
    /// A képek repoja.
    /// </summary>
    public interface IKepRepository : IRepository<Kep>
    {
        /// <summary>
        /// Elforgatja a megadott kép tárolt tájolását.
        /// </summary>
        /// <param name="kep">Az elforgatandó kép.</param>
        void Elforgat(int kep);

        /// <summary>
        /// Szerkeszt egy képet.
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="rendelesiKod">Rendelési kód.</param>
        /// <param name="eleresiUt">Elérési út.</param>
        /// <param name="fotos">Fotós.</param>
        /// <param name="tajolas">Tájolás.</param>
        /// <returns>Igaz, ha sikeres.</returns>
        bool EditKep(int id, string rendelesiKod, string eleresiUt, string fotos, decimal tajolas);
    }
}
