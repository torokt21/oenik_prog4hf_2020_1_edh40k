﻿// <copyright file="IRendelesInfoRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhotoOrder.Data;

    /// <summary>
    /// A rendeléssel végrehajtott műveltek interface.
    /// </summary>
    public interface IRendelesInfoRepository : IRepository<Rendeles_info>
    {
        /// <summary>
        /// Az megadott rendeléshez tartozó osztályt változtatja meg.
        /// </summary>
        /// <param name="rendeles_id">A rendelés infó id-je.</param>
        /// <param name="osztaly">Az új osztály.</param>
        void UpdateOsztaly(int rendeles_id, string osztaly);
    }
}
