﻿// <copyright file="IRepository.cs" company="TThread">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A hagyományos repository interface az alap CRUD funciókkal.
    /// </summary>
    /// <typeparam name="T">Az elemek típusa.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Az összes elemet adja vissza.
        /// </summary>
        /// <returns>Visszaadja az összes elemet.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Beilleszt egy új elemet a gyűjteménybe.
        /// </summary>
        /// <param name="entity">Az új elem.</param>
        void Insert(T entity);

        /// <summary>
        /// Törli a megadott nevet.
        /// </summary>
        /// <param name="entity">Az törlendő entitás.</param>
        void Remove(T entity);
    }
}
