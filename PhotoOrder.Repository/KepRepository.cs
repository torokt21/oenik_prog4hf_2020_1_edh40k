﻿// <copyright file="KepRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Repository
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using PhotoOrder.Data;

    /// <summary>
    /// A képeket kezelő repository interface.
    /// </summary>
    public class KepRepository : Repository<Kep>, IKepRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KepRepository"/> class.
        /// </summary>
        /// <param name="context">A context.</param>
        public KepRepository(DbContext context)
            : base(context)
        {
        }

        /// <inheritdoc/>
        public void Elforgat(int kep_id)
        {
            Kep kep = this.GetAll().First(k => k.kep_id == kep_id);
            kep.tajolas = kep.tajolas == 0 ? 1 : 0;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public bool EditKep(int id, string rendelesiKod, string eleresiUt, string fotos, decimal tajolas)
        {
            Kep kep = this.GetAll().FirstOrDefault(k => k.kep_id == id);

            if (kep == default)
            {
                return false;
            }

            kep.rendelesi_kod = rendelesiKod;
            kep.eleresi_ut = eleresiUt;
            kep.fotos = fotos;
            kep.tajolas = tajolas;
            this.Context.SaveChanges();
            return true;
        }
    }
}
