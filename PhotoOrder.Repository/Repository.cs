﻿// <copyright file="Repository.cs" company="TThread">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhotoOrder.Data;

    /// <summary>
    /// Standard repository.
    /// </summary>
    /// <typeparam name="T">Az elemek típusa.</typeparam>
    public class Repository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="context">A DbContext amiből az értékeket nyerjük ki.</param>
        public Repository(DbContext context)
        {
            this.Context = context;
        }

        /// <summary>
        /// Gets the adatbázis context.
        /// </summary>
        protected DbContext Context { get; }

        /// <inheritdoc/>
        public IQueryable<T> GetAll()
        {
            return this.Context.Set<T>();
        }

        /// <inheritdoc/>
        public void Insert(T entity)
        {
            this.Context.Set<T>().Add(entity);
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void Remove(T entity)
        {
            this.Context.Set<T>().Remove(entity);
            this.Context.SaveChanges();
        }
    }
}
