﻿// <copyright file="RendelesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhotoOrder.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhotoOrder.Data;

    /// <summary>
    /// A rendelések kezelését megkövetelő interface.
    /// </summary>
    public class RendelesRepository : Repository<Rendeles>, IRendelesRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RendelesRepository"/> class.
        /// </summary>
        /// <param name="context">A db context.</param>
        public RendelesRepository(DbContext context)
            : base(context)
        {
        }

        /// <inheritdoc/>
        public void UpdateMennyiseg(int rendelesElemId, int mennyiseg)
        {
            Rendeles rendeles = this.GetAll().First(x => x.rendeles_elem_id == rendelesElemId);

            rendeles.darabszam = mennyiseg;
            this.Context.SaveChanges();
        }
    }
}
