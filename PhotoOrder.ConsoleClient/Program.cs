﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace PhotoOrder.ConsoleClient
{
    class Program
    {
        const string BaseUrl = "http://localhost:2592/api/KepApi/";
        static void Main(string[] args)
        {
            Console.WriteLine("Várakozás... Nyomj meg egy gombot, amikor az ASP elindult!");
            Console.ReadKey();

            using (HttpClient client = new HttpClient())
            {
                Console.WriteLine("------------ READ --------------");

                string json = client.GetStringAsync(BaseUrl + "all").Result;
                var list = JsonConvert.DeserializeObject<List<KepModel>>(json); ;

                list.ForEach(k => Console.WriteLine(k));

                Console.WriteLine("------------ CREATE --------------");

                Dictionary<string, string> postData = new Dictionary<string, string>();
                postData.Add(nameof(KepModel.Fotos), "Kovács András");
                postData.Add(nameof(KepModel.EleresiUt), "storage/picture/folder/");
                postData.Add(nameof(KepModel.RendelesiKod), "1928374651");
                postData.Add(nameof(KepModel.Tajolas), "Álló");

                string response = client.PostAsync(BaseUrl + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                Console.WriteLine(response);

                json = client.GetStringAsync(BaseUrl + "all").Result;
                list = JsonConvert.DeserializeObject<List<KepModel>>(json); ;

                list.ForEach(k => Console.WriteLine(k));

                Console.WriteLine("------------ UPDATE --------------");

                int kepId = list.First(k => k.RendelesiKod == "1928374651").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(KepModel.Id), kepId.ToString());
                postData.Add(nameof(KepModel.Fotos), "Professor Kovács András");
                postData.Add(nameof(KepModel.EleresiUt), "storage/picture/folder/");
                postData.Add(nameof(KepModel.RendelesiKod), "1928374651");
                postData.Add(nameof(KepModel.Tajolas), "Fekvő");

                response = client.PostAsync(BaseUrl + "update", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                Console.WriteLine(response);

                json = client.GetStringAsync(BaseUrl + "all").Result;
                list = JsonConvert.DeserializeObject<List<KepModel>>(json); ;

                list.ForEach(k => Console.WriteLine(k));

                Console.WriteLine("------------ DELETE --------------");
                json = client.GetStringAsync(BaseUrl + "delete/" + kepId).Result;

                json = client.GetStringAsync(BaseUrl + "all").Result;
                list = JsonConvert.DeserializeObject<List<KepModel>>(json); ;

                list.ForEach(k => Console.WriteLine(k));
            }

            Console.WriteLine("Kész");
            Console.ReadKey();
        }
    }
}
