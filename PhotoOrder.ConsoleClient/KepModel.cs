﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoOrder.ConsoleClient
{
    class KepModel
    {
        public int Id { get; set; }

        public string RendelesiKod { get; set; }

        public string Fotos { get; set; }

        public string EleresiUt { get; set; }

        public string Tajolas { get; set; }

        public override string ToString()
        {
            return $"{Id} - {RendelesiKod} - {Tajolas} kép by {Fotos} ({EleresiUt})";
        }
    }
}
